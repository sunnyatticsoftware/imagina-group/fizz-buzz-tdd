#!/bin/bash

echo "Creating solution structure with projects.."

dotnet new sln
dotnet new gitignore
dotnet new classlib --name FizzBuzz --output src/FizzBuzz
dotnet new xunit --name FizzBuzz.UnitTests --output test/FizzBuzz.UnitTests
dotnet add test/FizzBuzz.UnitTests package FluentAssertions
dotnet add test/FizzBuzz.UnitTests reference src/FizzBuzz
dotnet sln add src/FizzBuzz
dotnet sln add test/FizzBuzz.UnitTests
