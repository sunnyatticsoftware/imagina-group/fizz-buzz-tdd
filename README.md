# fizz-buzz

Ejemplo para demostrar los fundamentos básicos de TDD y red-green-refactor

Utilizando TDD crear una funcionalidad que, dado un número, se devuelve un texto
- Fizz si el número es divisible por 3
- Buzz si el número es divisible por 5
- FizzBuzz si el número es divisible por 3 y por 5
- El número si no es divisible ni por 3 ni por 5

## Crear solución
Ver y ejecutar `create-structure.sh` o `create-structure.bat`, o ejecutar cada línea por separado para crear la estructura de solución y proyectos.

## Implemetación
Utiliza TDD para, partiendo de los requisitos, ir implementando la solución siguiendo el ciclo red-green-refactor.