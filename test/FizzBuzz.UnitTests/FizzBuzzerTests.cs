using FluentAssertions;
using Xunit;

namespace FizzBuzz.UnitTests;

public class FizzBuzzerTests
{
    [Fact]
    public void Should_Return_1_When_1()
    {
        // Given
        var input = 1;
        var expectedResult = "1";

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
    
    [Fact]
    public void Should_Return_2_When_2()
    {
        // Given
        var input = 2;
        var expectedResult = "2";

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
    
    [Fact]
    public void Should_Return_Fizz_When_3()
    {
        // Given
        var input = 3;
        var expectedResult = "Fizz";

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
    
    [Fact]
    public void Should_Return_4_When_4()
    {
        // Given
        var input = 4;
        var expectedResult = "4";

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
    
    [Fact]
    public void Should_Return_Buzz_When_5()
    {
        // Given
        var input = 5;
        var expectedResult = "Buzz";

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
    
    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(4)]
    [InlineData(7)]
    public void Should_Return_Default_When_Non_Divisible_By_3_Or_5(int input)
    {
        // Given
        var expectedResult = input.ToString();

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
    
    [Theory]
    [InlineData(3)]
    [InlineData(6)]
    [InlineData(9)]
    public void Should_Return_Fizz_When_Divisible_By_3(int input)
    {
        // Given
        var expectedResult = "Fizz";

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
    
    [Theory]
    [InlineData(5)]
    [InlineData(10)]
    public void Should_Return_Buzz_When_Divisible_By_5(int input)
    {
        // Given
        var expectedResult = "Buzz";

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
    
    [Theory]
    [InlineData(15)]
    [InlineData(30)]
    public void Should_Return_FizzBuzz_When_Divisible_By_3_And_5(int input)
    {
        // Given
        var expectedResult = "FizzBuzz";

        // When
        string result = FizzBuzzer.GetValue(input);

        // Then
        result.Should().Be(expectedResult);
    }
}