namespace FizzBuzz;

public class FizzBuzzer
{
    public static string GetValue(int input)
    {
        var result = string.Empty;

        if (input % 3 == 0)
        {
            result += "Fizz";
        }
        
        if (input % 5 == 0)
        {
            result += "Buzz";
        }

        if (string.IsNullOrEmpty(result))
        {
            return input.ToString();
        }

        return result;
    }
}